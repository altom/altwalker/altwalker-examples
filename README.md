# AltWalker Examples

> **Warning**: **This repository was moved to GitHub, you can find the new repository at: https://github.com/altwalker/altwalker-examples.**

This repository holds examples of tests that can be executed with AltWalker.

Read more [here](https://altom.gitlab.io/altwalker/altwalker/examples.html).

Join our Gitter chat room [here](https://gitter.im/altwalker/community) to chat with us or with other members of the community.

## Table of Contents

* Python Examples:
    * [E-Commerce Demo](/python-ecommerce/README.md)
    * [Debugger Example](/python-debugger/README.md)
    * [Config Example](/python-config/README.md)
    * [Python Authentication Module](/python-auth/README.md)
* C#/.NET Examples:
    * [E-Commerce Demo](/dotnet-ecommerce/README.md)
