stages:
  - lint
  - check-models
  - test

image: altwalker/altwalker:latest

python-debugger-lint:
  stage: lint
  before_script:
    - cd python-debugger
    - pip3 install flake8
  script:
    - flake8 --max-line-length=120 tests

python-debugger-check-models:
  stage: check-models
  before_script:
    - cd python-debugger
  script:
    - altwalker check -m models/debug.json "random(never)"

python-config-lint:
  stage: lint
  before_script:
    - cd python-config
    - pip3 install flake8
  script:
    - flake8 --max-line-length=120 tests

python-config-check-models:
  stage: check-models
  before_script:
    - cd python-config
  script:
    - altwalker check -m models/model.json "random(never)"

python-ecommerce-lint:
  stage: lint
  before_script:
    - cd python-ecommerce
    - pip3 install flake8
  script:
    - flake8 --max-line-length=120 tests

python-ecommerce-check-models:
  stage: check-models
  before_script:
    - cd python-ecommerce
  script:
    - altwalker check -m models/navigation.json "random(never)" -m models/checkout.json "random(never)"

python-ecommerce-tests:
  stage: test
  retry: 2
  before_script:
    - source install_firefox.sh
    - cd python-ecommerce
    - pip3 install -r requirements.txt --user
  script:
    - altwalker verify -m models/navigation.json tests
    - altwalker online -m models/navigation.json "random(length(40))" tests
    - altwalker online -m models/navigation.json "random(length(20))" -m models/checkout.json "random(vertex_coverage(100))" tests
  artifacts:
    paths:
      - "*.log"
    when: always

dotnet-ecommerce-check-models:
  stage: check-models
  before_script:
    - cd dotnet-ecommerce
  script:
    - altwalker check -m models/navigation.json "random(never)" -m models/checkout.json "random(never)"

dotnet-ecommerce-tests:
  stage: test
  image: altwalker/tests:dotnet-v2.2
  retry: 2
  before_script:
    - source install_firefox.sh
    - pip3 install --upgrade pip setuptools wheel
    - pip3 install idna==2.8
    - pip3 install altwalker
    - cd dotnet-ecommerce
  script:
    - dotnet build tests
    - altwalker verify -l c# -m models/navigation.json -m models/checkout.json tests
    - altwalker online -l c# -m models/navigation.json "random(length(40))" tests
    - altwalker online -l c# -m models/navigation.json "random(length(20))" -m models/checkout.json "random(vertex_coverage(100))" tests
  artifacts:
    paths:
      - "*.log"
    when: always

python-auth-lint:
  stage: lint
  before_script:
    - cd python-auth
    - pip3 install flake8
  script:
    - flake8 --max-line-length=120 tests

python-auth-check-models:
  stage: check-models
  before_script:
    - cd python-auth
  script:
    - altwalker check -m models/models.json "random(never)"

python-auth-tests:
  stage: test
  retry: 2
  services:
    - name: altwalker/demos:django-auth
      alias: django-auth
  before_script:
    - source install_firefox.sh
    - cd python-auth
    - pip3 install -r requirements.txt --user
  script:
    - export APP_HOST=django-auth
    - altwalker verify -m models/models.json tests
    - altwalker online -m models/models.json "random(edge_coverage(100))" tests
  artifacts:
    paths:
      - "*.log"
      - python-auth/screenshots/*.png
    when: always
